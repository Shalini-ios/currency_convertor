//  CurrencyConverter.swift

import Foundation

class CurrencyConverter {
    
    private var exchangeRates : [Currency : Double] = [:]
    private let xmlParser = CurrencyXMLParser()
    
    // Initialization:
    // init() { updateExchangeRates {} }
    
    //Updates the exchange rate
    public func updateExchangeRates(completion : @escaping () -> Void = {}) {
        xmlParser.parse(completion: {
            self.exchangeRates = self.xmlParser.getExchangeRates()
            completion()
        }, errorCompletion: {
        })
    }
    
    // Converts a Double value based on it's currency
    public func convert(_ value : Double, valueCurrency : Currency, outputCurrency : Currency) -> Double? {
        guard let valueRate = exchangeRates[valueCurrency] else { return nil }
        guard let outputRate = exchangeRates[outputCurrency] else { return nil }
        let multiplier = outputRate/valueRate
        return value * multiplier
    }
    
    
    // Converts a Double value based on it's currency and the output currency, and returns a formatted String.
    public func convertAndFormat(_ value : Double, valueCurrency : Currency, outputCurrency : Currency) -> String? {
        
        guard let doubleOutput = convert(value, valueCurrency: valueCurrency, outputCurrency: outputCurrency) else {
            return nil
        }
        return format(doubleOutput, numberStyle: .decimal, decimalPlaces: 4)
    }
    
    //Returns a formatted string from a double value.
    public func format(_ value : Double, numberStyle : NumberFormatter.Style, decimalPlaces : Int) -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = numberStyle
        formatter.maximumFractionDigits = decimalPlaces
        return formatter.string(from: NSNumber(value: value))
    }
}

private class CurrencyXMLParser : NSObject, XMLParserDelegate {
    
    private let xmlURL = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
    private var exchangeRates : [Currency : Double] = [
        .AUD : 1.0 // Base currency
    ]
    
    public func getExchangeRates() -> [Currency : Double] {
        return exchangeRates
    }
    
    public func parse(completion : @escaping () -> Void, errorCompletion : @escaping () -> Void) {
        guard let url = URL(string: xmlURL) else { return }
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                errorCompletion()
                return
            }
            let parser = XMLParser(data: data)
            parser.delegate = self
            if parser.parse() {
                completion()
            } else {
                errorCompletion()
            }
        }
        task.resume()
    }
    
    private func makeExchangeRate(currency : String, rate : String) -> (currency : Currency, rate : Double)? {
        guard let currency = Currency(rawValue: currency) else { return nil }
        guard let rate = Double(rate) else { return nil }
        return (currency, rate)
    }
    
    // XML Parse Methods (from XMLParserDelegate):
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "Cube"{
            guard let currency = attributeDict["currency"] else { return }
            guard let rate = attributeDict["rate"] else { return }
            guard let exchangeRate = makeExchangeRate(currency: currency, rate: rate) else { return }
            exchangeRates.updateValue(exchangeRate.rate, forKey: exchangeRate.currency)
        }
    }
    
}
