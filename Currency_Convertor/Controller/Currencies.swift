//
//  Currencies.swift
//  Currency_Convertor
//
//  Created by 922235 on 24/02/20.
//  Copyright © 2020 922235. All rights reserved.
//

import Foundation

enum Currency : String, CaseIterable {
    
    case INR = "INR"; case VND = "VND"; case ZAR = "ZAR"
    case USD = "USD"; case SAR = "SAR"; case VUV = "VUV"
    case BRL = "BRL"; case JPY = "JPY"; case AED = "AED"
    case CAD = "CAD"; case KRW = "KRW"; case TWD = "TWD"
    case CHF = "CHF"; case WST = "WST"; case TOP = "TOP"
    case CNY = "CNY"; case MYR = "MYR"; case BDT = "BDT"
    case NOK = "NOK"; case PGK = "PGK"; case FJD = "FJD"
    case DKK = "DKK"; case NZD = "NZD"; case SBD = "SBD"
    case EUR = "EUR"; case PHP = "PHP"; case XPF = "XPF"
    case GBP = "GBP"; case CLP = "CLP"; case XAU = "XAU"
    case HKD = "HKD"; case ARS = "ARS"; case AUD = "AUD"
    case CNH = "CNH"; case PKR = "PKR"
    case SEK = "SEK"; case BND = "BND"
    case IDR = "IDR"; case SGD = "SGD"
    case THB = "THB"; case LKR = "LKR"
    
}
