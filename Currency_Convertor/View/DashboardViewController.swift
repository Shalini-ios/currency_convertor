//
//  DashboardViewController.swift
//  Currency_Convertor
//
//  Created by 922235 on 20/02/20.
//  Copyright © 2020 922235. All rights reserved.
//

import UIKit
import Foundation

class DashboardViewController: UIViewController, ChangeCurrencyViewControllerDelegate {
    
    
    @IBOutlet weak private var exchangeRatesLbl: UILabel!
    @IBOutlet weak private var australiaBtn: UIButton!
    @IBOutlet weak private var scrollView: UIScrollView!
    @IBOutlet weak private var dataTableView: UITableView!
    @IBOutlet weak private var convertButton: UIButton!
    @IBOutlet weak private var amountField: UITextField!
    @IBOutlet weak private var currencyButton: UIButton!
    @IBOutlet weak private var convertedAmountLbl: UILabel!
    @IBOutlet weak private var definitionLbl: UILabel!
    @IBOutlet weak private var lastUpdatedLbl: UILabel!
    
    private let currencyConverter = CurrencyConverter()
    private let cellReuseIdentifier = "cell"
    private var selectedCurrencyCode: String = ""
    public var selectedIndex: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectedCurrencyCode = "INR"
        exchangeRatesLbl.text = String(format: "Foriegn Exchange Rates: %@", "INR")
        exchangeRatesLbl.isHidden = true
        currencyButton.setTitle(String(format: "%@  %@", "🇮🇳", "INR"), for: .normal)
        australiaBtn.setTitle(String(format: "%@  %@", "🇦🇺", "AUD"), for: .normal)
        self.dataTableView.isHidden = true
        self.dataTableView.isUserInteractionEnabled = false
        
        fetchCurrencyDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        amountField.text = ""
    }
    
    private func fetchCurrencyDetails() {
        APIHandler.shared.fetchCurrencyDetails(completion: {
            DispatchQueue.main.async {
                self.exchangeRatesLbl.isHidden = false
                self.dataTableView.isHidden = false
                self.dataTableView.delegate = self
                self.dataTableView.dataSource = self
                self.dataTableView.layer.borderWidth = 1
                self.dataTableView.layer.borderColor = UIColor.black.cgColor
                self.dataTableView.layer.cornerRadius = 5
                self.dataTableView.reloadData()
            }
        })
        
    }
    
    // Convert button action handler
    @IBAction func convertBtnTap(_ sender: Any) {
        self.amountField.resignFirstResponder()
        convertCurrency()
    }
    
    // Convert currency implemenation
    func convertCurrency() {
        currencyConverter.updateExchangeRates(completion: {
            
            DispatchQueue.main.async {
                let inputAmount = Double(self.amountField.text!)
                if self.amountField.text != "" {
                    let currencyConvertedString = self.currencyConverter.convertAndFormat(inputAmount!, valueCurrency: Currency(rawValue: self.selectedCurrencyCode) ?? Currency(rawValue: "")!, outputCurrency: .AUD)
                    if currencyConvertedString == nil {
                        self.convertedAmountLbl.isHidden = true
                        self.definitionLbl.isHidden = true
                        self.lastUpdatedLbl.isHidden = true
                        self.showAlert(message: "Currency rate not available")
                    }else {
                        self.convertedAmountLbl.isHidden = false
                        self.definitionLbl.isHidden = false
                        self.lastUpdatedLbl.isHidden = false
                        
                        self.convertedAmountLbl.text = currencyConvertedString
                        self.definitionLbl.text = String(format:"%@ %@ equals %@ AUD", self.amountField.text!, self.selectedCurrencyCode, currencyConvertedString!)
                    }
                }else {
                    self.convertedAmountLbl.isHidden = true
                    self.definitionLbl.isHidden = true
                    self.lastUpdatedLbl.isHidden = true
                    self.showAlert(message: "Please enter the amount")
                    
                }
                
                
            }
        })
    }
    
    //Method to present alert
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // delegate method for selected currency
    func didChangeCurrency(_ currencyCode: String, countryFlag: String, selectedIndex: Int) {
        self.selectedCurrencyCode = currencyCode
        self.selectedIndex = selectedIndex
        currencyButton.setTitle(String(format: "%@   %@", countryFlag, currencyCode), for: .normal)
        exchangeRatesLbl.text = String(format: "Foriegn Exchange Rates: %@", currencyCode)
        loadCurrencyValue()
    }
    
    // Segue to navigate to country tableview
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let countryTableViewController = segue.destination as! CountryTableViewController
        countryTableViewController.delegate = self
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: scrollView.contentSize.height + 500)
    }
}

extension DashboardViewController {
    
    private func loadCurrencyValue() {
        self.dataTableView.reloadData()
    }
}
