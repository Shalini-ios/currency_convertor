//
//  CountryTableViewController.swift
//  Currency_Convertor
//
//  Created by 922235 on 21/02/20.
//  Copyright © 2020 922235. All rights reserved.
//

import Foundation
import UIKit

protocol ChangeCurrencyViewControllerDelegate {
    func didChangeCurrency(_ currencyCode: String, countryFlag: String, selectedIndex: Int)
}

class CountryTableViewController: UITableViewController  {
    
    var delegate: ChangeCurrencyViewControllerDelegate?
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APIHandler.shared.currencyModelObject.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        
        let  selectedCountryFlag: String = APIHandler.shared.currencyModelObject[indexPath.row].countryFlag
        let  selectedCountry: String = APIHandler.shared.currencyModelObject[indexPath.row].country
        let  selectedCurrencyCode: String = APIHandler.shared.currencyModelObject[indexPath.row].currencyCode
        
        cell.textLabel?.text = String(format: " %@  %@ - %@", selectedCountryFlag, selectedCountry, selectedCurrencyCode)
        cell.detailTextLabel?.text = APIHandler.shared.currencyModelObject[indexPath.row].currencyName
        cell.detailTextLabel?.textColor = UIColor.systemBlue
        cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let  selectedCountryFlag: String = APIHandler.shared.currencyModelObject[indexPath.row].countryFlag
        let  currencyConvertion: String = Currency.init(rawValue: APIHandler.shared.currencyModelObject[indexPath.row].currencyCode)!.rawValue
        
        delegate?.didChangeCurrency(currencyConvertion, countryFlag: selectedCountryFlag, selectedIndex: indexPath.row)
        self.navigationController?.popViewController(animated: true)
    }
}
