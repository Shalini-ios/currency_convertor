//
//  ViewController+Delegates.swift
//  Currency_Convertor
//
//  Created by 922235 on 22/02/20.
//  Copyright © 2020 922235. All rights reserved.
//

import Foundation
import UIKit

extension DashboardViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        let selectedIndexValue: CurrencyMapper = APIHandler.shared.currencyModelObject[selectedIndex]
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        if indexPath.row == 0 {
            cell.textLabel?.text = String(format: "Buy TC:        %@", selectedIndexValue.buyTC)
        }else if indexPath.row == 1 {
            cell.textLabel?.text = String(format: "Buy TT:        %@", selectedIndexValue.buyTT)
        }else if indexPath.row == 2 {
            cell.textLabel?.text = String(format: "Sell TT:        %@", selectedIndexValue.sellTT)
        }else if indexPath.row == 3 {
            cell.textLabel?.text = String(format: "Buy Notes: %@", selectedIndexValue.buyNotes)
        }else {
            cell.textLabel?.text = String(format: "Sell Notes: %@", selectedIndexValue.sellNotes)
        }
        return cell
    }
}
