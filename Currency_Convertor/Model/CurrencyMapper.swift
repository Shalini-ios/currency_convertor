//
//  CurrencyMapper.swift
//  Currency_Convertor
//
//  Created by 922235 on 21/02/20.
//  Copyright © 2020 922235. All rights reserved.
//

import UIKit

public struct CurrencyMapper {
    let lastUpdated: String
    let spotRateDateFmt: String
    let buyNotes: String
    let buyTC: String
    let buyTT: String
    let country: String
    let currencyCode: String
    let currencyName: String
    let effectDateFmt: String
    let sellNotes: String
    let sellTT: String
    let updateDateFmt: String
    let countryFlag: String
    
    init?(dict: [String: Any]) {
        guard let lastUpdated = dict["LASTUPDATED"] as? String,
            let spotRateDateFmt = dict["SpotRate_Date_Fmt"] as? String,
            let buyNotes = dict["buyNotes"] as? String,
            let buyTC = dict["buyTC"] as? String,
            let buyTT = dict["buyTT"] as? String,
            let country = dict["country"] as? String,
            let currencyCode = dict["currencyCode"] as? String,
            let currencyName = dict["currencyName"] as? String,
            let effectDateFmt = dict["effectiveDate_Fmt"] as? String,
            let sellNotes = dict["sellNotes"] as? String,
            let sellTT = dict["sellTT"] as? String,
            let updateDateFmt = dict["updateDate_Fmt"] as? String,
            let countryFlag = dict["countryFlag"] as? String
            else { return nil }
        
        self.lastUpdated = lastUpdated
        self.spotRateDateFmt = spotRateDateFmt
        self.buyNotes = buyNotes
        self.buyTC = buyTC
        self.buyTT = buyTT
        self.country = country
        self.currencyCode = currencyCode
        self.currencyName = currencyName
        self.effectDateFmt = effectDateFmt
        self.sellNotes = sellNotes
        self.sellTT = sellTT
        self.updateDateFmt = updateDateFmt
        self.countryFlag = countryFlag
    }
    
    
}
