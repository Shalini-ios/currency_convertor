//
//  APIHandler.swift
//  Currency_Convertor
//
//  Created by 922235 on 21/02/20.
//  Copyright © 2020 922235. All rights reserved.
//

import Foundation


class APIHandler {
    
    static let shared = APIHandler()
    private var responseDictionary = NSDictionary()
    private var keyValueArray = NSMutableArray()
    var currencyModelObject = [CurrencyMapper]()
    static let requestUrl = URL(string: "https://www.westpac.com.au/bin/getJsonRates.wbc.fx.json")
    
    // API handler to fetch data
    func fetchCurrencyDetails(completion: @escaping () -> Void = {}) {
        
        guard let url = APIHandler.requestUrl else {
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                return
            }
            guard let responseData = data else {
                return
            }
            
            // parse the result as JSON
            do {
                guard let responseData = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        return
                }
                
                let data = responseData["data"] as! NSDictionary
                let brands = data["Brands"] as! NSDictionary
                let wbc = brands["WBC"] as! NSDictionary
                let brand = wbc["Portfolios"] as! NSDictionary
                let FX = brand["FX"] as! NSDictionary
                
                self.responseDictionary = FX["Products"] as! NSDictionary
                
                for (_,value) in self.responseDictionary {
                    self.keyValueArray.add(value)
                }
                
                for dictinary in self.keyValueArray
                {
                    let dictVal: NSDictionary = dictinary as! NSDictionary
                    let productid: String = dictVal.object(forKey: "ProductId") as! String
                    let ratesDict: NSDictionary = dictVal["Rates"] as! NSDictionary
                    var currentDict: [String : Any] = ratesDict[productid] as! [String : Any]
                    let countryCodePrefix = currentDict["currencyCode"] as! String
                    currentDict["countryFlag"] = String.emojiFlag(for: String(countryCodePrefix.prefix(2)))
                    guard let post = CurrencyMapper(dict: currentDict ) else { continue }
                    
                    self.currencyModelObject.append(post)
                }
                completion()
            } catch  {
                return
            }
        }
        task.resume()
        
    }
    
}

extension String {
    
    // Method to get emoji flag from country code
    static func emojiFlag(for countryCode: String) -> String! {
        func isLowercaseASCIIScalar(_ scalar: Unicode.Scalar) -> Bool {
            return scalar.value >= 0x61 && scalar.value <= 0x7A
        }
        
        func regionalIndicatorSymbol(for scalar: Unicode.Scalar) -> Unicode.Scalar {
            precondition(isLowercaseASCIIScalar(scalar))
            return Unicode.Scalar(scalar.value + (0x1F1E6 - 0x61))!
        }
        
        let lowercasedCode = countryCode.lowercased()
        guard lowercasedCode.count == 2 else { return nil }
        guard lowercasedCode.unicodeScalars.reduce(true, { accum, scalar in accum && isLowercaseASCIIScalar(scalar) }) else { return nil }
        
        let indicatorSymbols = lowercasedCode.unicodeScalars.map({ regionalIndicatorSymbol(for: $0) })
        return String(indicatorSymbols.map({ Character($0) }))
    }
}
